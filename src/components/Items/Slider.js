import 'swiper/css';
import { Swiper, SwiperSlide } from 'swiper/react';
import { newInStore } from '../../data';
import { EffectCoverflow } from 'swiper/modules';

const Slider = () => {
     return (
          <Swiper
               initialSlide={1}
               effect={'coverflow'}
               centeredSlides={true}
               slidesPerView={'auto'}
               coverflowEffect={{
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: true,
               }}
               modules={[EffectCoverflow]}
               grabCursor={true}
               breakpoints={{
                    320: {
                         slidesPerView: 2,
                         spaceBetween: 18,
                    },
                    768: {
                         slidesPerView: 3,
                         spaceBetween: 20,
                    },
               }}
          >
               {newInStore.products.map((product, index) => {
                    return (
                         <SwiperSlide className='max-w-[265px]' key={index}>
                              <div className='relative'>
                                   <img src={product.image.type} alt='' />
                                   <div className='absolute text-white bottom-[20px] w-full text-center text-[18px] lg:text-2xl font-medium capitalize'>
                                        {product.name}
                                   </div>
                              </div>
                         </SwiperSlide>
                    );
               })}
          </Swiper>
     );
};

export default Slider;
