import Hero from './Hero';
import Stats from './Stats';
import Features from './Features';
import SecondFeatures from './SecondFeatures';
import Products from './Products';
import Testimonials from './Testimonials';
import Newsletter from './Newsletter';
import Footer from './Footer';
import Header from './Header';

export { Hero, Stats, Features, SecondFeatures, Products, Testimonials, Newsletter, Footer, Header };
