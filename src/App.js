import './index.css';
import { Features, Footer, Header, Hero, Newsletter, Products, SecondFeatures, Testimonials } from './components';
import Items from './components/Items';

const App = () => {
     return (
          <div className='w-full max-w-[1440px] mx-auto bg-white'>
               <Header />
               <Hero />
               <Features />
               <Items />
               <SecondFeatures />
               <Products />
               <Testimonials />
               <Newsletter />
               <Footer />
          </div>
     );
};

export default App;
